#!/bin/zsh

# TODO : Make this configurable (with a file that if present will take precendence over these defaults)
export DOTFILES_PUBLIC="$HOME/.dotfiles_public"
export DOTFILES_PRIVATE="$HOME/.dotfiles_private"

for script in $DOTFILES_PUBLIC/modules/zsh/init/*.zsh; do
	source $script
done

source_if_exists $HOME/.zshrc_private
