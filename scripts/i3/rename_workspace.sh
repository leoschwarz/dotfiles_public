#!/bin/bash
# @author Leonardo Schwarz <leoschwarz.com>
# @install requires jq and zenity packages to be installed
# @description Creates a prompt to rename the current i3 workspace, useful to manage large numbers of workspaces (>10).
#   Despite using the zenity gui prompt it's not necessary to use a mouse.

old_name=`i3-msg -t get_workspaces \
  | jq '.[] | select(.focused==true).name' \
  | cut -d"\"" -f2` # source https://faq.i3wm.org/question/6200/obtain-info-on-current-workspace-etc.1.html
new_name=`zenity --entry --entry-text "$old_name" --text "New workspace name"`

if [ $? -eq 0 ]; then
    i3-msg rename workspace to "\"$new_name\""
else
    echo "Info: Didn't provide target workspace name."
fi
